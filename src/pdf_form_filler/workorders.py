import dataclasses
import itertools
import re
from pathlib import Path
from typing import Self

from reportlab.pdfgen.canvas import Canvas

Dataset = list[dict]

@dataclasses.dataclass(frozen=True)
class Point:
    x: int
    y: int


@dataclasses.dataclass(frozen=True)
class Transformation:
    text: str
    font: str
    page: int
    size: int
    position: Point

    @classmethod
    def from_dict(cls, a_dict: dict) -> Self:
        return cls(
            text=a_dict["text"],
            font=a_dict["font"],
            page=a_dict["page"],
            size=a_dict["size"],
            position=Point(a_dict["position"][0], a_dict["position"][1]),
        )

    def apply_on(self, canvas: Canvas, data: dict):
        canvas.setFont(self.font, self.size)
        canvas.drawString(self.position.x, self.position.y, self.text.format(**data))


@dataclasses.dataclass(frozen=True)
class Instruction:
    template: Path
    output: str
    transformations: list[Transformation]
    working_dir: Path
    output_dir: Path

    @classmethod
    def from_dict(cls, a_dict: dict) -> Self:
        return cls(
            template=Path(a_dict["input"]),
            output=a_dict["output"],
            transformations=[
                Transformation.from_dict(transformation_dict)
                for transformation_dict in a_dict["transformations"]
            ],
            working_dir=a_dict["working_dir"],
            output_dir=a_dict["output_dir"],
        )

    def get_template_path(self) -> Path:
        if self.template.exists():
            return self.template
        return self.working_dir / self.template

    def get_output_path(self, data: dict) -> Path:
        context = data.copy()
        context["input"] = self.template.name
        filename = re.sub(r"[^a-zA-Z0-9/,. ]", "-", self.output.format(**context))
        return self.output_dir / Path(filename)

    def get_transformations_by_page(self):
        transformations = sorted(self.transformations, key=lambda x: x.page)
        return itertools.groupby(transformations, key=lambda x: x.page)


@dataclasses.dataclass(frozen=True)
class WorkOrder:
    working_dir: Path
    output_dir: Path
    instructions: list[Instruction]

    @classmethod
    def from_dict(cls, a_dict: dict) -> Self:
        working_dir = Path(a_dict.get("working_dir", Path.cwd()))
        output_dir = Path(a_dict.get("output_dir", working_dir / "output"))

        for instruction in a_dict["instructions"]:
            instruction.setdefault("working_dir", working_dir)
            instruction.setdefault("output_dir", output_dir)

        return cls(
            instructions=[Instruction.from_dict(instruction_dict) for instruction_dict in a_dict["instructions"]],
            working_dir=working_dir,
            output_dir=output_dir,
        )
