import sys

from pdf_form_filler.cli import main

if __name__ == "__main__":
    sys.exit(main())
