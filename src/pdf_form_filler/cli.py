import argparse
import csv
import logging
import sys
import tomllib
from pathlib import Path

from pdf_form_filler.concurrency import generate_reports
from pdf_form_filler.workorders import WorkOrder


def main(argv=None):
    logging.basicConfig(level=logging.INFO, format="%(asctime)s %(levelname)s [%(name)s] %(message)s")

    parser = argparse.ArgumentParser(description="Fill PDF reports automatically")
    parser.add_argument("-w", "--work-order", required=True)
    parser.add_argument("-d", "--data", required=True)
    parser.add_argument("-o", "--output-dir", required=True)

    args = parser.parse_args(argv)
    work_order_path = Path(args.work_order)
    output_dir_path = Path(args.output_dir)
    data_path = Path(args.data)

    work_order = get_work_order_from_file(work_order_path, output_dir_path)
    data_set = get_dataset_from_file(data_path)
    generate_reports(work_order, data_set)


def get_work_order_from_file(work_order_path, output_dir_path):
    output_dir = Path(output_dir_path)
    instructions = tomllib.loads(work_order_path.read_text())
    instructions.setdefault("output_dir", output_dir)
    work_order = WorkOrder.from_dict(instructions)
    return work_order


def get_dataset_from_file(data_path):
    with data_path.open("r") as open_file:
        csv_reader = csv.DictReader(open_file)
        return list(csv_reader)


if __name__ == "__main__":
    sys.exit(main())
