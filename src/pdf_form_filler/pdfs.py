import io
import logging
import os
import pathlib

from pypdf import PdfWriter, PdfReader
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen.canvas import Canvas

from pdf_form_filler.workorders import Instruction

logger = logging.getLogger(__name__)

DEBUG = os.getenv("PDF_FORM_FILLER_DEBUG")
DEBUG_MAX_X = 1000
DEBUG_MAX_Y = 1000
DEBUG_X_INTERVAL = 100
DEBUG_Y_INTERVAL = 50


def generate_pdf(template: io.BytesIO, instruction: Instruction, data: dict):
    new_pages = _build_new_pages(instruction, data)
    output = _merge_pdfs(template, new_pages)
    return _write_output(output, instruction, data)


def _build_new_pages(instruction: Instruction, data: dict) -> dict[int, io.BytesIO]:
    return {
        page_number: _build_page(transformations_in_page, data)
        for page_number, transformations_in_page in instruction.get_transformations_by_page()
    }


def _build_page(transformations_in_page, data):
    """Create a new PDF with Reportlab"""
    buffer = io.BytesIO()
    canvas = Canvas(buffer, pagesize=letter)

    for transformation in transformations_in_page:
        transformation.apply_on(canvas, data)

    if DEBUG:
        for x in range(0, DEBUG_MAX_X, DEBUG_X_INTERVAL):
            for y in range(0, DEBUG_MAX_Y, DEBUG_Y_INTERVAL):
                canvas.drawString(x, y, f"{x},{y}")

    canvas.save()
    return buffer


def _merge_pdfs(input_file: io.BytesIO, pages: dict[int, io.BytesIO]) -> PdfWriter:
    existing_pdf = PdfReader(input_file)
    output = PdfWriter()
    for page_number, existing_page in enumerate(existing_pdf.pages):
        if new_page := pages.get(page_number):
            existing_page.merge_page(PdfReader(new_page).pages[0])
        output.add_page(existing_page)
    return output


def _write_output(output: PdfWriter, instruction: Instruction, data: dict) -> pathlib.Path:
    output_path = instruction.get_output_path(data)
    output_path.parent.mkdir(parents=True, exist_ok=True)
    with output_path.open("wb") as output_file:
        output.write(output_file)
    return output_path
