import concurrent.futures
import functools
import logging
import os

from pdf_form_filler import pdfs
from pdf_form_filler.workorders import WorkOrder, Dataset, Instruction

logger = logging.getLogger(__name__)

__existing_pdf = None

MAX_PROCESS_WORKERS = 8


def generate_reports(work_order: WorkOrder, data_set: Dataset):
    for instruction in work_order.instructions:
        _generate_pdfs(data_set, instruction)


def _generate_pdfs(data_set: Dataset, instruction: Instruction):
    with concurrent.futures.ProcessPoolExecutor(max_workers=MAX_PROCESS_WORKERS,
                                                initializer=_initialize_worker,
                                                initargs=(instruction,)) as executor:
        for output_path in executor.map(functools.partial(_generate_pdf, instruction), data_set):
            logger.info('Done: %s', output_path)


def _initialize_worker(instruction: Instruction):
    """Black magic to avoid read the same file for each item in the dataset"""
    global __existing_pdf
    logging.basicConfig(level=logging.INFO, format="%(asctime)s %(levelname)s [%(name)s] %(message)s")
    logger.info("Initializing worker %d", os.getpid())
    __existing_pdf = instruction.get_template_path().open("rb")


def _generate_pdf(instruction: Instruction, data: dict):
    return pdfs.generate_pdf(__existing_pdf, instruction, data)
