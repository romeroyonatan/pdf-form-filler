import textwrap
from pathlib import Path

from pdf_form_filler.cli import main

DATA_DIR = Path(__file__).parent / "data"
FORM_1040 = DATA_DIR / "f1040.pdf"

def test_generate_pdfs(tmp_path):
    work_order = tmp_path / "work-order.toml"
    data_set = tmp_path / "data-set.csv"

    work_order.write_text(f"""
    [[instructions]]
    input = '{FORM_1040}'
    output = "{{surname}}, {{name}}/{{input}}"

    [[instructions.transformations]]
    text = "{{surname}}"
    font = "Helvetica"
    size = 9
    page = 0
    position = [244,664]
    """)

    data_set.write_text(textwrap.dedent("""\
    surname,name
    Doe,John
    Schmidt,Anna
    """))

    output_dir = tmp_path / "output"

    main(["--work-order", str(work_order), "--data", str(data_set), "--output-dir", str(output_dir)])

    assert (output_dir / "Doe, John" / "f1040.pdf").exists()
    assert (output_dir / "Schmidt, Anna" / "f1040.pdf").exists()
